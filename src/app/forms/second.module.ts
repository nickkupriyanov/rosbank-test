import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecondComponent } from './second.component';
import { SecondRoutingModule } from './second-routing.module';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';

@NgModule({
    imports: [
        SecondRoutingModule,
        CommonModule,
        Angular2FontawesomeModule,
    ],
    exports: [],
    declarations: [SecondComponent],
    providers: [],
})
export class SecondModule { }
