import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-second',
    templateUrl: 'second.component.html',
    styleUrls: ['second.component.scss']
})

export class SecondComponent implements OnInit {
    tabs = [
        {
            title: 'Параметры претензии',
            text: 'Контент для Параметры претензии'
        },
        {
            title: 'Досье Клиента',
            text: 'Контент для Досье Клиента'
        },
        {
            title: 'Результат рассмотрения',
            text: 'Контент для Результат рассмотрения'
        },
        {
            title: 'Контроль качества',
            text: 'Контент для Контроль качества'
        },
        {
            title: '',
            text: 'Контент для '
        },
        {
            title: '...',
            text: 'Контент для ...'
        }
    ];
    currentTab;

    constructor() {
        this.currentTab = this.tabs[0];
     }

    ngOnInit() { }

    onTabClick(tab) {
        this.currentTab = tab;
    }
}
