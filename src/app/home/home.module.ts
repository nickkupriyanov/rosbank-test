import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { MenuItemComponent } from '../menu-item/menu-item.component';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';

@NgModule({
    imports: [
        HomeRoutingModule,
        CommonModule,
        Angular2FontawesomeModule,
    ],
    exports: [
        CommonModule,
    ],
    declarations: [
        HomeComponent,
        MenuItemComponent,
    ],
    providers: [],
})
export class HomeModule { }
