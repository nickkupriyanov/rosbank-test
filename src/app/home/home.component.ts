import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit {
    folders = [
        {
            title: 'Расширенный поиск'
        },
        {
            title: 'Зарегистрированные с превышением срока (12)'
        },
        {
            title: 'Новые обращения (144)'
        },
        {
            title: 'В работе',
            children: [
                {
                    title: 'срок истек (12)'
                },
                {
                    title: 'срок не истек (34)'
                }
            ]
        },
        {
            title: 'Обработанные'
        },
        {
            title: 'Распределенные',
            children: [
                {
                    title: 'срок истек (34)'
                },
                {
                    title: 'срок не истек (45)'
                }
            ]
        },
        {
            title: 'Закрытые'
        },
        {
            title: 'Ожидают ответа на запрос',
            children: [
                {
                    title: 'срок истек (1)'
                },
                {
                    title: 'срок не истек (3)'
                }
            ]
        },
        {
            title: 'Ожидают согласования',
            children: [
                {
                    title: 'срок истек (1)'
                },
                {
                    title: 'срок не истек (3)'
                }
            ]
        },
        {
            title: 'Ожидают отправки документов (2)'
        },

    ];
    foldersSecond = [
        {
            title: 'Руководство пользователя',
            children: [
                {
                    title: 'подключение'
                },
                {
                    title: 'смена роли'
                }
            ]
        },
        {
            title: 'Нормативные документы'
        },
        {
            title: 'Типовые формы бланков'
        },
        {
            title: 'др.'
        },
    ];
    constructor() { }

    ngOnInit() { }

    onMenuItemClick(item) {
        if (item.children) {
            console.log(item);
        }
    }
}
